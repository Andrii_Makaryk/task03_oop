package com.makayk;

public abstract class Article {
    private String name;
    private Department department;
    private int price;

    public Article() {

    }

    public Article(String name, Department department, int price) {
        this.name = name;
        this.department = department;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public Department getDepartment() {
        return department;
    }

    public int getPrice() {
        return price;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Article{" +
                "name='" + name + '\'' +
                ", department='" + department + '\'' +
                ", price=" + price +
                '}';
    }
}
