package com.makayk;

public class PlumbingStuff extends Article{
    private String nameOfMatirial;

    public PlumbingStuff() {

    }

    public PlumbingStuff(String name, Department department, int price, String nameOfMatirial) {
        super(name, department, price);
        this.nameOfMatirial = nameOfMatirial;
    }

    public String getNameOfPoint() {
        return nameOfMatirial;
    }

    public void setNameOfPoint(String nameOfMatirial) {
        this.nameOfMatirial = nameOfMatirial;
    }

    @Override
    public String toString() {
        return "PlumbingStuff{" +
                "nameOfPoint='" + nameOfMatirial + '\'' +
                '}';
    }
}
