package com.makayk;

public class Wooden extends Article {
    private String nameOfWood;

    public Wooden() {

    }

    public Wooden(String nameOfWood) {
        this.nameOfWood = nameOfWood;
    }

    public Wooden(String name, Department department, int price, String nameOfWood) {
        super(name, department, price);
        this.nameOfWood = nameOfWood;
    }


    public String getNameOfWood() {
        return nameOfWood;
    }

    public void setNameOfWood(String nameOfWood) {
        this.nameOfWood = nameOfWood;
    }

    @Override
    public String toString() {
        return "Wooden{" +
                "nameOfWood='" + nameOfWood + '\'' +
                '}';
    }
}
