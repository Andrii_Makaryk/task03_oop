package com.makayk;

public class Paints extends Article {
    private int volume;

    public Paints() {
    }

    public Paints(String name, Department department, int price, int volume) {
        super(name, department, price);
        this.volume = volume;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    @Override
    public String toString() {
        return "Paints{" +
                "volume=" + volume +
                '}';
    }
}
