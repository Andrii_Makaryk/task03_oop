package market;

import com.makayk.*;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;


public class Market {
    private List<Article> store;

    {
        Article tableOne = new Wooden("TableOne", Department.Wooden_products, 34589, "Oak");
        Article tableTwo = new Wooden("TableTwo", Department.Wooden_products, 24999, "Linden");
        Article chairOne = new Wooden("ChearOne", Department.Wooden_products, 4567, "Maple");
        Article chairTwo = new Wooden("ChearTwo", Department.Wooden_products, 1567, "Pine");
        Article wardrobe = new Wooden("Wardrobe", Department.Wooden_products, 14999, "Linden");
        Article commode = new Wooden("Commode", Department.Wooden_products, 27999, "Pine");
        Article bowlOne = new PlumbingStuff("BowlOne", Department.Plumbing, 16000, "Сeramics");
        Article bowlTwo = new PlumbingStuff("BowlTwo", Department.Plumbing, 10000, "Сeramics");
        Article bathOne = new PlumbingStuff("BathOne", Department.Plumbing, 17000, "castIron");
        Article bathTwo = new PlumbingStuff("BathTwo", Department.Plumbing, 16799, "Ceramics");
        Article washbasinOne = new PlumbingStuff("WashbasinOne", Department.Plumbing, 13000, "castIron");
        Article washbasinTwo = new PlumbingStuff("WashbasinTwo", Department.Plumbing, 7999, "Ceramics");
        Article washbasinThree = new PlumbingStuff("WashbasinThree", Department.Plumbing, 10000, "castIron");
        Article washbasinFour = new PlumbingStuff("WashbasinFour", Department.Plumbing, 16999, "Ceramics");
        Article washbasinFive = new PlumbingStuff("WashbasinFive", Department.Plumbing, 5000, "castIron");
        Article washbasinSix = new PlumbingStuff("WashbasinSix", Department.Plumbing, 9999, "Ceramics");
        Article varnishOne = new Paints("VarnishOne", Department.Paints_varnishes, 120, 500);
        Article varnishTwo = new Paints("VarnishTwo", Department.Paints_varnishes, 450, 1000);
        Article paintOne = new Paints("PaintOne", Department.Paints_varnishes, 200, 500);
        Article paintTwo = new Paints("PaintOne", Department.Paints_varnishes, 170, 500);
        store = new ArrayList<Article>();
        store.add(tableOne);
        store.add(tableTwo);
        store.add(chairOne);
        store.add(chairTwo);
        store.add(wardrobe);
        store.add(commode);
        store.add(bowlOne);
        store.add(bowlTwo);
        store.add(bathOne);
        store.add(bathTwo);
        store.add(washbasinOne);
        store.add(washbasinTwo);
        store.add(washbasinThree);
        store.add(washbasinFour);
        store.add(washbasinFive);
        store.add(washbasinSix);
        store.add(varnishOne);
        store.add(varnishTwo);
        store.add(paintOne);
        store.add(paintTwo);
    }

    public void find(String name, int price) {
        store.stream().filter(p -> p.getName().contains(name) && p.getPrice() <= price).forEach(p -> {
            System.out.println("Name: " + p.getName());
            System.out.println("Price: " + p.getPrice());
            System.out.println("Department: " + p.getDepartment());
            System.out.println("===================================" );
        });
    }
}
